# image_segmentation

1. Модель по распознаванию текста (work in progress): https://github.com/roman-4erkasov/cv/tree/main/task03_ocr/prj03_ctc
2. Модель по сегментации автомобильных номеров (work in progress): https://github.com/roman-4erkasov/cv/tree/main/task02_localization/prj06_car_plate_segmentation
3. Модель по сегментации замазанных мест на карте на основе датасета из kaggle: https://github.com/roman-4erkasov/cv/blob/main/task02_localization/prj04_unet/project_03_torch/
Детекция и сегментация (insatnce segmentation) пешеходов с помощью MaskRCNN (Detectron2): https://github.com/roman-4erkasov/cv/blob/main/task02_localization/prj03_mrcnn_torchvision/Untitled1.ipynb
